A repository for various demo jupyter notebooks.

Try them out immediately here: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.gwdg.de%2Fniels.cautaerts%2Fjupyterdemos/master?filepath=CTF%20Demo.ipynb)

Current (10/06/2020) contents:

* interactive contrast transfer function demo
* demonstration/calculation of fringe spacings in 2-beam CBED for thickness determinations